# coding=utf-8
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import os
import base64
import datetime
import streamlit as st

import joblib
from sklearn.preprocessing import StandardScaler
from lightgbm import LGBMClassifier

ABSOLUTE_PATH = os.path.dirname(__file__)


def title():
    st.title('Предсказание дождя на завтрашний день')
    st.image('https://cdn.pixabay.com/photo/2016/05/19/22/57/raindrops-1404209_1280.jpg',
             use_column_width=True)


def page_description_data():
    title()

    st.write(
        '## Исходный датасет'
    )

    st.write(
        'Исходный датасет имеет множество различных метеорологический данных, например, '
        'минимальная температура, максимальная температура, давление, скорость ветра, '
        'место в котором проводились измерения, был ли сегодня дождь и многое другое.'
    )

    st.write(
        '## Обработанный датасет'
    )

    st.write(
        'Первое что бросается в глаза после проведения первичного '
        'анализа данных, в датасете имеется большое количество пропущенных '
        'значений, удалив которые, мы бы потеряли минимум половину датасета, '
        'следовательно, необходимо их заполнить.'
    )

    st.write(
        'Пропуски в данных признаках были заполнены с '
        'помощью стандартных методов заполнения пропусков в машинном обучении, '
        'а именно заполнением самым популярным вариантом, для категориальных '
        'признаков, и заполнением средним значением, для вещественных.'
    )

    st.write(
        'После заполнения пропущенных значений необходимо обработать признаки. '
        'Признак агрегат с датой `Date` был преобразован к типу `Datetime` и '
        'разделён на такие признаки как, год, месяц, день и время года.'
    )

    st.write(
        'Наконец, осталось масштабировать вещественные признаки и '
        'применить one-hot кодирование к категориальным и можно приступать '
        'к построению моделей.'
    )


def page_description_models():
    title()

    st.write('## Построение моделей')

    st.markdown(
        '''
        В качестве моделей для построения были выбраны следующие 
        модели, как наиболее продвинутые:
        
        - _LogisticRegression_
        - _Naive Bayes_
        - _RandomForest_
        - _XGBoost_
        - _LightGBM_
        
        Данные модели и их композиции часто дают лучшие результаты для задач классификации.
        '''
    )

    st.write(
        'Проведя глубокий анализ выбранных обученных моделей, '
        'была выбрана модель LGBMClassifier из библиотеки lightgbm в качестве основной.'
    )


def page_using_model():
    title()

    st.write('## Использование модели')

    input_data_method = st.radio('Выберите способ ввода данных:',
                                 ('Ввести вручную', 'Загрузить файл с данными'))

    if input_data_method == 'Ввести вручную':
        input_manual()
    elif input_data_method == 'Загрузить файл с данными':
        input_file()


def input_manual():
    columns_date = st.beta_columns(3)
    today = datetime.date.today()
    day = columns_date[0].slider(
        'День',
        min_value=1,
        max_value=31,
        value=today.day
    )
    month = columns_date[1].slider(
        'Месяц',
        min_value=1,
        max_value=12,
        value=today.month
    )
    year = columns_date[2].number_input('Год', value=today.year, step=1, min_value=1500, max_value=2500)
    try:
        Date = datetime.date(year, month, day)
        st.success('Корректные параметры')
    except ValueError as e:
        st.error(f'В {month} месяце нет {day} дня в {year} году!')

    Location = st.selectbox('Location:',
                            ('Unknown', 'Albury', 'BadgerysCreek', 'Cobar', 'CoffsHarbour', 'Moree',
                             'Newcastle', 'NorahHead', 'NorfolkIsland', 'Penrith', 'Richmond',
                             'Sydney', 'SydneyAirport', 'WaggaWagga', 'Williamtown',
                             'Wollongong', 'Canberra', 'Tuggeranong', 'MountGinini', 'Ballarat',
                             'Bendigo', 'Sale', 'MelbourneAirport', 'Melbourne', 'Mildura',
                             'Nhil', 'Portland', 'Watsonia', 'Dartmoor', 'Brisbane', 'Cairns',
                             'GoldCoast', 'Townsville', 'Adelaide', 'MountGambier', 'Nuriootpa',
                             'Woomera', 'Albany', 'Witchcliffe', 'PearceRAAF', 'PerthAirport',
                             'Perth', 'SalmonGums', 'Walpole', 'Hobart', 'Launceston',
                             'AliceSprings', 'Darwin', 'Katherine', 'Uluru'))

    columns_temp = st.beta_columns(2)
    MinTemp = columns_temp[0].number_input('Мин. температура °C:', step=0.1)
    MaxTemp = columns_temp[1].number_input('Макс. температура °C:', step=0.1)
    if MaxTemp < MinTemp:
        st.error('Максимальная температура не может быть меньше минимальной!')
    elif MinTemp > 45:
        st.error('Минимальная температура слишком высокая!')
    elif MaxTemp > 45:
        st.error('Максимальная температура слишком высокая!')
    elif MinTemp < -10:
        st.error('Минимальная температура слишком низкая!')
    elif MaxTemp < -10:
        st.error('Максимальная температура слишком низкая!')
    else:
        st.success('Корректные параметры')

    block_res = st.beta_expander('Дождь, испарение, солнечный свет', True)
    block_res_columns_Rainfall = block_res.beta_columns((4, 1))
    Rainfall = block_res_columns_Rainfall[0].slider(
        'Rainfall',
        min_value=0.0,
        max_value=6.0,
        step=0.2
    )
    if block_res_columns_Rainfall[1].checkbox('Значение неизвестно', key='block_res_columns_Rainfall'):
        Rainfall = np.nan

    block_res_columns_Evaporation = block_res.beta_columns((4, 1))
    Evaporation = block_res_columns_Evaporation[0].slider(
        'Evaporation',
        min_value=0.0,
        max_value=30.0,
        step=0.2
    )
    if block_res_columns_Evaporation[1].checkbox('Значение неизвестно', key='block_res_columns_Evaporation'):
        Evaporation = np.nan

    block_res_columns_Sunshine = block_res.beta_columns((4, 1))
    Sunshine = block_res_columns_Sunshine[0].slider(
        'Sunshine',
        min_value=0.0,
        max_value=15.0,
        step=0.2
    )
    if block_res_columns_Sunshine[1].checkbox('Значение неизвестно', key='block_res_columns_Sunshine'):
        Sunshine = np.nan

    sides_of_the_horizon = ['Unknown', 'W', 'WNW', 'WSW', 'NE', 'NNW', 'N', 'NNE', 'SW', 'ENE',
                            'SSE', 'S', 'NW', 'SE', 'ESE', 'E', 'SSW']
    block_wind = st.beta_expander('Ветер', True)
    WindGustDir = block_wind.selectbox('WindGustDir:', sides_of_the_horizon)
    block_wind_columns_WindGustDir = block_wind.beta_columns((4, 1))
    WindGustSpeed = block_wind_columns_WindGustDir[0].slider(
        'WindGustSpeed',
        min_value=6,
        max_value=100
    )
    if block_wind_columns_WindGustDir[1].checkbox('Значение неизвестно', key='block_wind_columns_WindGustDir'):
        WindGustSpeed = np.nan
    WindDir9am = block_wind.selectbox('WindDir9am:', sides_of_the_horizon)
    WindDir3pm = block_wind.selectbox('WindDir3pm:', sides_of_the_horizon)
    block_wind_columns_WindSpeed9am = block_wind.beta_columns((4, 1))
    WindSpeed9am = block_wind_columns_WindSpeed9am[0].slider(
        'WindSpeed9am',
        min_value=6,
        max_value=60
    )
    if block_wind_columns_WindSpeed9am[1].checkbox('Значение неизвестно', key='block_wind_columns_WindSpeed9am'):
        WindSpeed9am = np.nan
    block_wind_columns_WindSpeed3pm = block_wind.beta_columns((4, 1))
    WindSpeed3pm = block_wind_columns_WindSpeed3pm[0].slider(
        'WindSpeed3pm',
        min_value=6,
        max_value=60
    )
    if block_wind_columns_WindSpeed3pm[1].checkbox('Значение неизвестно', key='block_wind_columns_WindSpeed3pm'):
        WindSpeed3pm = np.nan

    block_humidity = st.beta_expander('Влажность', True)
    block_humidity_columns_Humidity9am = block_humidity.beta_columns((4, 1))
    Humidity9am = block_humidity_columns_Humidity9am[0].slider(
        'Humidity9am',
        min_value=6,
        max_value=100
    )
    if block_humidity_columns_Humidity9am[1].checkbox('Значение неизвестно', key='block_humidity_columns_Humidity9am'):
        Humidity9am = np.nan
    block_humidity_columns_Humidity3pm = block_humidity.beta_columns((4, 1))
    Humidity3pm = block_humidity_columns_Humidity3pm[0].slider(
        'Humidity3pm',
        min_value=6,
        max_value=100
    )
    if block_humidity_columns_Humidity3pm[1].checkbox('Значение неизвестно', key='block_humidity_columns_Humidity3pm'):
        Humidity3pm = np.nan

    block_pressure = st.beta_expander('Давление', True)
    block_pressure_columns_Pressure9am = block_pressure.beta_columns((4, 1))
    Pressure9am = block_pressure_columns_Pressure9am[0].slider(
        'Pressure9am',
        min_value=980.0,
        max_value=1040.0,
        step=0.1
    )
    if block_pressure_columns_Pressure9am[1].checkbox('Значение неизвестно', key='block_pressure_columns_Pressure9am'):
        Pressure9am = np.nan
    block_pressure_columns_Pressure3pm = block_pressure.beta_columns((4, 1))
    Pressure3pm = block_pressure_columns_Pressure3pm[0].slider(
        'Pressure3pm',
        min_value=980.0,
        max_value=1040.0,
        step=0.1
    )
    if block_pressure_columns_Pressure3pm[1].checkbox('Значение неизвестно', key='block_pressure_columns_Pressure3pm'):
        Pressure3pm = np.nan

    block_cloud = st.beta_expander('Облачность', True)
    block_cloud_columns_Cloud9am = block_cloud.beta_columns((4, 1))
    Cloud9am = block_cloud_columns_Cloud9am[0].slider(
        'Cloud9am',
        min_value=0,
        max_value=9
    )
    if block_cloud_columns_Cloud9am[1].checkbox('Значение неизвестно', key='block_cloud_columns_Cloud9am'):
        Cloud9am = np.nan
    block_cloud_columns_Cloud3pm = block_cloud.beta_columns((4, 1))
    Cloud3pm = block_cloud_columns_Cloud3pm[0].slider(
        'Cloud3pm',
        min_value=0,
        max_value=9
    )
    if block_cloud_columns_Cloud3pm[1].checkbox('Значение неизвестно', key='block_cloud_columns_Cloud3pm'):
        Cloud3pm = np.nan

    block_temp = st.beta_expander('Температура', True)
    block_temp_columns_Temp9am = block_temp.beta_columns((4, 1))
    Temp9am = block_temp_columns_Temp9am[0].slider(
        'Temp9am',
        min_value=-10.0,
        max_value=45.0,
        step=0.1
    )
    if block_temp_columns_Temp9am[1].checkbox('Значение неизвестно', key='block_temp_columns_Temp9am'):
        Temp9am = np.nan
    block_temp_columns_Temp3pm = block_temp.beta_columns((4, 1))
    Temp3pm = block_temp_columns_Temp3pm[0].slider(
        'Temp3pm',
        min_value=-10.0,
        max_value=45.0,
        step=0.1
    )
    if block_temp_columns_Temp3pm[1].checkbox('Значение неизвестно', key='block_temp_columns_Temp3pm'):
        Temp3pm = np.nan

    RainToday = st.selectbox('RainToday:', ('Unknown', 'Yes', 'No'))

    button_predict = st.button('Получить прогноз дождя на завтрашний день!')
    if button_predict:
        data_dict = \
            {
                'Date': Date, 'Location': Location, 'MinTemp': MinTemp, 'MaxTemp': MaxTemp, 'Rainfall': Rainfall,
                'Evaporation': Evaporation, 'Sunshine': Sunshine, 'WindGustDir': WindGustDir,
                'WindGustSpeed': WindGustSpeed,
                'WindDir9am': WindDir9am, 'WindDir3pm': WindDir3pm, 'WindSpeed9am': WindSpeed9am,
                'WindSpeed3pm': WindSpeed3pm,
                'Humidity9am': Humidity9am, 'Humidity3pm': Humidity3pm, 'Pressure9am': Pressure9am,
                'Pressure3pm': Pressure3pm,
                'Cloud9am': Cloud9am, 'Cloud3pm': Cloud3pm, 'Temp9am': Temp9am, 'Temp3pm': Temp3pm,
                'RainToday': RainToday
            }

        df = pd.DataFrame(data=data_dict, index=[0])
        df.replace('Unknown', np.nan, inplace=True)

        st.write('### Полученный объект:')
        st.write(df)
        st.write('Размерность датафрейма {}'.format(df.shape))
        st.write('### Данные после обработки')
        df = data_preprocessing(df)
        st.write(df)
        st.write('Размерность датафрейма {}'.format(df.shape))
        model_prediction(df)


def input_file():
    st.write("Файл для примера:")
    with open((ABSOLUTE_PATH + '/data/test_data.csv'), 'rb') as test_data:
        b64 = base64.b64encode(test_data.read()).decode()
        st.markdown(f'<a href="data:file/csv;base64,{b64}" download="test_data.csv">Скачать файл</a>',
                    unsafe_allow_html=True)

    file = st.file_uploader(label='Загрузите данные для предсказания', type=['csv'], accept_multiple_files=False)
    if file is not None:
        df = pd.read_csv(file)
        st.write('### Исходные данные')
        st.write(df)
        st.write('Размерность датафрейма {}'.format(df.shape))
        st.write('### Данные после обработки')
        df = data_preprocessing(df)
        st.write(df)
        st.write('Размерность датафрейма {}'.format(df.shape))
        model_prediction(df)


def data_preprocessing(df: pd.DataFrame) -> pd.DataFrame:
    if 'RainTomorrow' in df.columns:
        df.drop(columns='RainTomorrow', inplace=True)

    # Объединение с исходными данными для дальнейшей обработки входных данных
    df_len = len(df)
    df = df.append(load_data_for_pretreatment(), ignore_index=True)

    # Обработка данных
    df['Date'] = pd.to_datetime(df['Date'])
    df['month'] = df['Date'].dt.month
    df['seasons'] = 4
    df.loc[np.logical_and(np.array(3 <= df['month']), np.array(df['month'] <= 5)), 'seasons'] = 1
    df.loc[np.logical_and(np.array(6 <= df['month']), np.array(df['month'] <= 8)), 'seasons'] = 2
    df.loc[np.logical_and(np.array(9 <= df['month']), np.array(df['month'] <= 11)), 'seasons'] = 3
    df.drop(columns=['Date'], inplace=True)

    int_feat = ['MinTemp', 'MaxTemp', 'Rainfall', 'Evaporation', 'Sunshine',
                'WindGustSpeed', 'WindSpeed9am', 'WindSpeed3pm', 'Humidity9am',
                'Humidity3pm', 'Pressure9am', 'Pressure3pm', 'Cloud9am', 'Cloud3pm',
                'Temp9am', 'Temp3pm', 'month', 'seasons']

    bool_feat = ['RainToday']

    df[int_feat] = df[int_feat].fillna(df[int_feat].mean())

    df['Location'] = df['Location'].fillna(df['Location'].mode()[0])
    df['WindGustDir'] = df['WindGustDir'].fillna(df['WindGustDir'].mode()[0])
    df['WindDir9am'] = df['WindDir9am'].fillna(df['WindDir9am'].mode()[0])
    df['WindDir3pm'] = df['WindDir3pm'].fillna(df['WindDir3pm'].mode()[0])

    df['RainToday'] = df['RainToday'].fillna(df['RainToday'].mode()[0])

    df[bool_feat] = df[bool_feat].replace(['No'], False)
    df[bool_feat] = df[bool_feat].replace(['Yes'], True)

    sc = StandardScaler()
    sc.fit(df[int_feat])
    df[int_feat] = sc.transform(df[int_feat])
    df = pd.get_dummies(df, drop_first=True)
    return df[:df_len]


def model_prediction(X: pd.DataFrame):
    st.write('### Предсказанные значения')
    model = load_model_lightgbm()
    pred = model.predict(X)
    del X
    pred = pd.DataFrame(pred, columns=['RainTomorrow'])
    st.write(pred)
    download_predictions(pred)


def download_predictions(pred: pd.DataFrame):
    st.markdown('### ⬇ Скачать CSV файл с предсказанием ⬇ ')
    b64 = base64.b64encode(pred.to_csv(index=False).encode()).decode()
    st.markdown(f'<a href="data:file/csv;base64,{b64}" download="predictions.csv">Скачать файл</a>',
                unsafe_allow_html=True)


def load_model_lightgbm():
    model = joblib.load(ABSOLUTE_PATH + '/models/lgb.pkl')
    return model


@st.cache
def load_data_for_pretreatment():
    df = pd.read_csv(ABSOLUTE_PATH + '/data/data_for_pretreatment.csv')
    return df


def sidebar():
    return st.sidebar.radio(
        'Выберите страницу:',
        ('Информация о датасете',
         'Информация о построенных моделях',
         'Использование модели'))


def main():
    st.set_page_config(page_title='Предсказание дождя на завтрашний день',
                       page_icon='https://iconarchive.com/download/i92064/icons8/windows-8/Weather-Rain.ico')
    st.markdown('<style>MainMenu {visibility: hidden;}footer {visibility: hidden;}</style>', unsafe_allow_html=True)

    st.sidebar.title('Страницы')
    page = sidebar()
    if page == 'Информация о датасете':
        page_description_data()
    elif page == 'Информация о построенных моделях':
        page_description_models()
    elif page == 'Использование модели':
        page_using_model()


if __name__ == '__main__':
    main()
