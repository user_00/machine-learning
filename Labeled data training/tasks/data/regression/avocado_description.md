# Данные о цене за авокадо

[Источник данных](https://www.kaggle.com/neuromusic/avocado-prices "Данные")

## Описание
Эти данные были загружены с веб-сайта Hass Avocado Board в мае 2018 года 
и скомпилированы в единый CSV-файл. Вот как [Hass Avocado Board описывает данные
на своем веб-сайте](https://hassavocadoboard.com/):

>The table below represents weekly 2018 retail scan data for National retail 
>volume (units) and price. Retail scan data comes directly from retailers’ 
>cash registers based on actual retail sales of Hass avocados. Starting in 2013, 
>the table below reflects an expanded, multi-outlet retail data set. Multi-outlet 
>reporting includes an aggregation of the following channels: grocery, mass, club, 
>drug, dollar and military. The Average Price (of avocados) in the table reflects 
>a per unit (per avocado) cost, even when multiple units (avocados) are sold in 
>bags. The Product Lookup codes (PLU’s) in the table are only for Hass avocados. 
>Other varieties of avocados (e.g. greenskins) are not included in this table.


## Варианты исследования
В качестве задачи исследования была выбрана задача регрессии.
Соответственно, целевой признак - признак 'AveragePrice'

## Признаки
* `Date` - дата наблюдения (порядковый)
* `AveragePrice` - средняя цена одного авокадо (числовой)
* `type` - обычные или органические (бинарный)
* `year` - год (порядковый)
* `Region` - город или район наблюдения (категориальный)
* `Total Volume` - общее количество проданных авокадо (числовой)
* `4046` - общее количество проданных авокадо с PLU 4046 (числовой)
* `4225` - общее количество проданных авокадо с PLU 4225 (числовой)
* `4770` - общее количество проданных авокадо с PLU 4770 (числовой)
