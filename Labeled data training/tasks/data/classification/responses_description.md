# Данные по опросу молодых людей

[Источник данных](https://www.kaggle.com/miroslavsabo/young-people-survey "Данные")

## Описание
В 2013 году студентов факультета статистики FSEV UK попросили пригласить 
своих друзей принять участие в этом опросе. Файл данных (response.csv) 
состоит из 1010 строк и 150 столбцов (139 целых и 11 категориальных). 
Для удобства исходные имена переменных в файле данных были сокращены. 
См. Файл columns.csv, если вы хотите сопоставить данные с исходными именами. 
Данные содержат пропущенные значения. Опрос был представлен участникам как в 
электронной, так и в письменной форме. Оригинал анкеты был на словацком языке, 
а затем был переведен на английский. Все участники были словацкими гражданами в 
возрасте от 15 до 30 лет. Переменные можно разделить на следующие группы:

* Music preferences (19 items)
* Movie preferences (12 items)
* Hobbies & interests (32 items)
* Phobias (10 items)
* Health habits (3 items)
* Personality traits, views on life, & opinions (57 items)
* Spending habits (7 items)
* Demographics (10 items)

## Варианты исследования
* Кластеризация
* Прогностическое моделирование
* Уменьшение размерности признаков
* Анализ корреляции
* Анализ пропущенных значений
* Построение рекомендательной системы

В качестве задачи исследования была выбрана задача бинарной классификации пола объекта.
Соответственно, целевой признак - признак 'Gender' 

## Опросник (версия, переведённая на английский)
#### MUSIC PREFERENCES
1) I enjoy listening to music.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) I prefer.: Slow paced music 1-2-3-4-5 Fast paced music (порядковый)
1) Dance, Disco, Funk: Don't enjoy at all 1-2-3-4-5 Enjoy very much (порядковый)
1) Folk music: Don't enjoy at all 1-2-3-4-5 Enjoy very much (порядковый)
1) Country: Don't enjoy at all 1-2-3-4-5 Enjoy very much (порядковый)
1) Classical: Don't enjoy at all 1-2-3-4-5 Enjoy very much (порядковый)
1) Musicals: Don't enjoy at all 1-2-3-4-5 Enjoy very much (порядковый)
1) Pop: Don't enjoy at all 1-2-3-4-5 Enjoy very much (порядковый)
1) Rock: Don't enjoy at all 1-2-3-4-5 Enjoy very much (порядковый)
1) Metal, Hard rock: Don't enjoy at all 1-2-3-4-5 Enjoy very much (порядковый)
1) Punk: Don't enjoy at all 1-2-3-4-5 Enjoy very much (порядковый)
1) Hip hop, Rap: Don't enjoy at all 1-2-3-4-5 Enjoy very much (порядковый)
1) Reggae, Ska: Don't enjoy at all 1-2-3-4-5 Enjoy very much (порядковый)
1) Swing, Jazz: Don't enjoy at all 1-2-3-4-5 Enjoy very much (порядковый)
1) Rock n Roll: Don't enjoy at all 1-2-3-4-5 Enjoy very much (порядковый)
1) Alternative music: Don't enjoy at all 1-2-3-4-5 Enjoy very much (порядковый)
1) Latin: Don't enjoy at all 1-2-3-4-5 Enjoy very much (порядковый)
1) Techno, Trance: Don't enjoy at all 1-2-3-4-5 Enjoy very much (порядковый)
1) Opera: Don't enjoy at all 1-2-3-4-5 Enjoy very much (порядковый)
#### MOVIE PREFERENCES
1) I really enjoy watching movies.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) Horror movies: Don't enjoy at all 1-2-3-4-5 Enjoy very much (порядковый)
1) Thriller movies: Don't enjoy at all 1-2-3-4-5 Enjoy very much (порядковый)
1) Comedies: Don't enjoy at all 1-2-3-4-5 Enjoy very much (порядковый)
1) Romantic movies: Don't enjoy at all 1-2-3-4-5 Enjoy very much (порядковый)
1) Sci-fi movies: Don't enjoy at all 1-2-3-4-5 Enjoy very much (порядковый)
1) War movies: Don't enjoy at all 1-2-3-4-5 Enjoy very much (порядковый)
1) Tales: Don't enjoy at all 1-2-3-4-5 Enjoy very much (порядковый)
1) Cartoons: Don't enjoy at all 1-2-3-4-5 Enjoy very much (порядковый)
1) Documentaries: Don't enjoy at all 1-2-3-4-5 Enjoy very much (порядковый)
1) Western movies: Don't enjoy at all 1-2-3-4-5 Enjoy very much (порядковый)
1) Action movies: Don't enjoy at all 1-2-3-4-5 Enjoy very much (порядковый)
#### HOBBIES & INTERESTS
1) History: Not interested 1-2-3-4-5 Very interested (порядковый)
1) Psychology: Not interested 1-2-3-4-5 Very interested (порядковый)
1) Politics: Not interested 1-2-3-4-5 Very interested (порядковый)
1) Mathematics: Not interested 1-2-3-4-5 Very interested (порядковый)
1) Physics: Not interested 1-2-3-4-5 Very interested (порядковый)
1) Internet: Not interested 1-2-3-4-5 Very interested (порядковый)
1) PC Software, Hardware: Not interested 1-2-3-4-5 Very interested (порядковый)
1) Economy, Management: Not interested 1-2-3-4-5 Very interested (порядковый)
1) Biology: Not interested 1-2-3-4-5 Very interested (порядковый)
1) Chemistry: Not interested 1-2-3-4-5 Very interested (порядковый)
1) Poetry reading: Not interested 1-2-3-4-5 Very interested (порядковый)
1) Geography: Not interested 1-2-3-4-5 Very interested (порядковый)
1) Foreign languages: Not interested 1-2-3-4-5 Very interested (порядковый)
1) Medicine: Not interested 1-2-3-4-5 Very interested (порядковый)
1) Law: Not interested 1-2-3-4-5 Very interested (порядковый)
1) Cars: Not interested 1-2-3-4-5 Very interested (порядковый)
1) Art: Not interested 1-2-3-4-5 Very interested (порядковый)
1) Religion: Not interested 1-2-3-4-5 Very interested (порядковый)
1) Outdoor activities: Not interested 1-2-3-4-5 Very interested (порядковый)
1) Dancing: Not interested 1-2-3-4-5 Very interested (порядковый)
1) Playing musical instruments: Not interested 1-2-3-4-5 Very interested (порядковый)
1) Poetry writing: Not interested 1-2-3-4-5 Very interested (порядковый)
1) Sport and leisure activities: Not interested 1-2-3-4-5 Very interested (порядковый)
1) Sport at competitive level: Not interested 1-2-3-4-5 Very interested (порядковый)
1) Gardening: Not interested 1-2-3-4-5 Very interested (порядковый)
1) Celebrity lifestyle: Not interested 1-2-3-4-5 Very interested (порядковый)
1) Shopping: Not interested 1-2-3-4-5 Very interested (порядковый)
1) Science and technology: Not interested 1-2-3-4-5 Very interested (порядковый)
1) Theatre: Not interested 1-2-3-4-5 Very interested (порядковый)
1) Socializing: Not interested 1-2-3-4-5 Very interested (порядковый)
1) Adrenaline sports: Not interested 1-2-3-4-5 Very interested (порядковый)
1) Pets: Not interested 1-2-3-4-5 Very interested (порядковый)
#### PHOBIAS
1) Flying: Not afraid at all 1-2-3-4-5 Very afraid of (порядковый)
1) Thunder, lightning: Not afraid at all 1-2-3-4-5 Very afraid of (порядковый)
1) Darkness: Not afraid at all 1-2-3-4-5 Very afraid of (порядковый)
1) Heights: Not afraid at all 1-2-3-4-5 Very afraid of (порядковый)
1) Spiders: Not afraid at all 1-2-3-4-5 Very afraid of (порядковый)
1) Snakes: Not afraid at all 1-2-3-4-5 Very afraid of (порядковый)
1) Rats, mice: Not afraid at all 1-2-3-4-5 Very afraid of (порядковый)
1) Ageing: Not afraid at all 1-2-3-4-5 Very afraid of (порядковый)
1) Dangerous dogs: Not afraid at all 1-2-3-4-5 Very afraid of (порядковый)
1) Public speaking: Not afraid at all 1-2-3-4-5 Very afraid of (порядковый)
#### HEALTH HABITS
1) Smoking habits: Never smoked - Tried smoking - Former smoker - Current smoker (категориальный)
1) Drinking: Never - Social drinker - Drink a lot (категориальный)
1) I live a very healthy lifestyle.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
#### PERSONALITY TRAITS, VIEWS ON LIFE & OPINIONS
1) I take notice of what goes on around me.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) I try to do tasks as soon as possible and not leave them until last minute.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) I always make a list so I don't forget anything.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) I often study or work even in my spare time.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) I look at things from all different angles before I go ahead.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) I believe that bad people will suffer one day and good people will be rewarded.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) I am reliable at work and always complete all tasks given to me.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) I always keep my promises.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) I can fall for someone very quickly and then completely lose interest.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) I would rather have lots of friends than lots of money.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) I always try to be the funniest one.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) I can be two faced sometimes.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) I damaged things in the past when angry.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) I take my time to make decisions.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) I always try to vote in elections.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) I often think about and regret the decisions I make.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) I can tell if people listen to me or not when I talk to them.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) I am a hypochondriac.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) I am emphatetic person.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) I eat because I have to. I don't enjoy food and eat as fast as I can.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) I try to give as much as I can to other people at Christmas.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) I don't like seeing animals suffering.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) I look after things I have borrowed from others.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) I feel lonely in life.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) I used to cheat at school.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) I worry about my health.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) I wish I could change the past because of the things I have done.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) I believe in God.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) I always have good dreams.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) I always give to charity.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) I have lots of friends.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) Timekeeping.: I am often early. - I am always on time. - I am often running late. (категориальный)
1) Do you lie to others?: Never. - Only to avoid hurting someone. - Sometimes. - Everytime it suits me. (категориальный)
1) I am very patient.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) I can quickly adapt to a new environment.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) My moods change quickly.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) I am well mannered and I look after my appearance.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) I enjoy meeting new people.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) I always let other people know about my achievements.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) I think carefully before answering any important letters.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) I enjoy childrens' company.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) I am not afraid to give my opinion if I feel strongly about something.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) I can get angry very easily.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) I always make sure I connect with the right people.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) I have to be well prepared before public speaking.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) I will find a fault in myself if people don't like me.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) I cry when I feel down or things don't go the right way.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) I am 100% happy with my life.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) I am always full of life and energy.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) I prefer big dangerous dogs to smaller, calmer dogs.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) I believe all my personality traits are positive.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) If I find something the doesn't belong to me I will hand it in.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) I find it very difficult to get up in the morning.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) I have many different hobbies and interests.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) I always listen to my parents' advice.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) I enjoy taking part in surveys.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) How much time do you spend online?: No time at all - Less than an hour a day - Few hours a day - Most of the day (категориальный)
#### SPENDING HABITS
1) I save all the money I can.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) I enjoy going to large shopping centres.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) I prefer branded clothing to non branded.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) I spend a lot of money on partying and socializing.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) I spend a lot of money on my appearance.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) I spend a lot of money on gadgets.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
1) I will hapilly pay more money for good, quality or healthy food.: Strongly disagree 1-2-3-4-5 Strongly agree (порядковый)
#### DEMOGRAPHICS
1) Age: (числовой)
1) Height: (числовой)
1) Weight: (числовой)
1) How many siblings do you have?: (числовой)
1) Gender: Female - Male (бинарный)
1) I am: Left handed - Right handed (бинарный)
1) Highest education achieved: Currently a Primary school pupil - Primary school - Secondary school - College/Bachelor degree (категориальный)
1) I am the only child: No - Yes (бинарный)
1) I spent most of my childhood in a: City - village (бинарный)
1) I lived most of my childhood in a: house/bungalow - block of flats (бинарный)

## Связанные публикации
* (на словацком языке) Sleziak, P. - Sabo, M .: Гендерные различия в 
распространенности конкретных фобий. Форум Statisticum Slovacum. 2014, 
Т. 10, № 6. [Различия (пол + проживали ли люди в поселке) в 
распространенности фобий.] 
* Сабо, Мирослав. Многомерные статистические методы 
с приложениями. Дисс. Словацкий технологический университет в Братиславе, 2014. 
[Кластеризация переменных (музыкальные предпочтения, 
предпочтения фильмов, фобии) + Кластеризация людей w.r.t. их интересы.]