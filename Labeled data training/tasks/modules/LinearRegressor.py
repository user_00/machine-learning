import numpy as np


class LinearRegressor:
    def __init__(self):
        self.w = None

    def fit(self, X, y):
        X = LinearRegressor.__add_bias(X)
        self.w = np.random.normal(size=(X.shape[1], 1))

        learn_rate = 0.01
        for epoch in range(100):
            for x, y_true in zip(X, y):
                self.w -= learn_rate * x.reshape(self.w.shape) * LinearRegressor.__error(y_true, np.dot(x, self.w), derivative=True)

        return self

    def predict(self, X):
        return np.dot(LinearRegressor.__add_bias(X), self.w).reshape(len(X))

    @staticmethod
    def __add_bias(X):
        X_new = np.zeros((X.shape[0], X.shape[1] + 1))
        X_new[:, :-1] = X
        X_new[:, -1] = 1
        return X_new

    @staticmethod
    def __error(y_true: float, y_pred: float, derivative=False) -> float:
        """
        Ошибка для конкретного объекта
        :param y_true: ответ (число или вектор)
        :param y_pred: предсказание (число или вектор)
        :param derivative: производная функции или нет
        :return ошибка (число или вектор соответственно)
        """

        if derivative:
            return -2 * (y_true - y_pred)
        return np.linalg.norm(y_true - y_pred) ** 2
