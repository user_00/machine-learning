import numpy as np
import matplotlib.pyplot as plt

# Функции активации
class ActivationFunction:
    @staticmethod
    def sigmoid(x: float, derivative=False) -> float:
        """
        Функция активации сигмоида
        :param x: аргумент (число или вектор)
        :param derivative: производная функции или нет
        :return значение функции
        """
        if derivative:
            # Производная сигмоиды: f'(x) = f(x) * (1 - f(x))
            return ActivationFunction.sigmoid(x) * (1 - ActivationFunction.sigmoid(x))
        # Сигмоидная функция активации: f(x) = 1 / (1 + e^(-x))
        return 1 / (1 + np.exp(-x))

    @staticmethod
    def hyperbolic_tangent(x: float, derivative=False) -> float:
        """
        Функция активации гиперболический тангенс
        :param x: аргумент (число или вектор)
        :param derivative: производная функции или нет
        :return значение функции
        """
        if derivative:
            return 1 - ActivationFunction.hyperbolic_tangent(x) ** 2
        return np.nan_to_num(np.exp(2 * x) - 1) / np.nan_to_num(np.exp(2 * x) + 1)

    @staticmethod
    def relu(x: float, derivative=False) -> float:
        """
        Функция активации ReLU
        :param x: аргумент (число или вектор)
        :param derivative: производная функции или нет
        :return значение функции
        """
        x = np.nan_to_num(x)
        if derivative:
            return (x / abs(x) + 1) / 2
        return np.nan_to_num((x + abs(x)) / 2)


# Функции ошибки
class ErrorFunction:
    @staticmethod
    def error(y_true: int, y_pred: float, derivative=False) -> float:
        """
        Ошибка для конкретного объекта
        :param y_true: ответ (число или вектор)
        :param y_pred: предсказание (число или вектор)
        :param derivative: производная функции или нет
        :return ошибка (число или вектор соответственно)
        """

        if derivative:
            return -2 * (y_true - y_pred)
        return np.linalg.norm(y_true - y_pred) ** 2


class NeuralNetworkClassifier:
    """
    Нейронная сеть
    """

    def __init__(self, number_neurons_hidden_layers: list, activation_functions: list):
        """
        :param number_neurons_hidden_layers - список с числом нейронов в каждом скрытом слое
        (первое число - число нейронов в первом скрытом слое, второе во втором и т.д.)
        :param activation_functions - активационные функции для каждого скрытого слоя и для выходного слоя
        """

        self.number_hidden_layers = len(number_neurons_hidden_layers)
        self.number_neurons_hidden_layers = number_neurons_hidden_layers

        # Функции активации (нулевое место не используем)
        self.act = [-1] + activation_functions
        # Функция ошибки
        self.err = ErrorFunction.error

        self.all_W = []
        self.B = []
        self.L = 0

    def predict(self, X) -> list:
        """
        :param X: вектор объектов, для которых просчитывается предсказания
        :return вектор выходных активаций для каждого объекта
        """
        y_pred = []
        for x in X:
            # Forward prop.:
            A = self.B.copy()
            # Пусть нулевые выходы будут признаками объекта
            A[0] = np.array(x).reshape((len(x), 1))
            for l in range(1, self.L):
                W = self.all_W[l]
                a = A[l - 1]
                b = self.B[l]
                fun_act = self.act[l]

                z = np.dot(W.T, a) + b
                A[l] = fun_act(z)
            y_pred.append(A[self.L - 1])
        return y_pred

    def fit(self, X, y, learn_rate=0.1, epochs=1000, plot_err_fun=False):
        """
        Тренируем…
        """

        number_neurons_input_layer = X.shape[1]
        number_neurons_output_layer = len(np.unique(y))
        # number_neurons_output_layer = len(np.unique(y)) - (1)

        # Список с количеством нейронов во всех слоях
        number_neurons_layers = self.number_neurons_hidden_layers
        number_neurons_layers.insert(0, number_neurons_input_layer)
        number_neurons_layers.append(number_neurons_output_layer)

        # Общее число слоёв (нумерация производится начиная с нулевого)
        L = len(number_neurons_layers)

        # Инициализируем матрицы весов, векторы смещений, векторы сумматоров нейронов, векторы выходов нейронов и
        # векторы ошибок сразу для всех слоёв
        all_W, B, Z, A, E = [], [], [], [], []
        for l in range(L):
            if l == 0:
                # Нулевое место не будем использовать
                all_W.append(-1)
                B.append(-1)
                continue

            all_W.append(2 * np.random.random(size=(number_neurons_layers[l - 1], number_neurons_layers[l])) - 1)
            B.append(2 * np.random.random(size=(number_neurons_layers[l], 1)) - 1)

        Z = B.copy()
        A = B.copy()
        E = B.copy()

        errors = []

        for epoch in range(epochs):
            for x, y_true in zip(X, y):
                # Forward prop.:
                # Пусть нулевые выходы будут признаками объекта
                A[0] = np.array(x).reshape((number_neurons_input_layer, 1))
                for l in range(1, L):
                    W = all_W[l]
                    a = A[l - 1]
                    b = B[l]
                    fun_act = self.act[l]

                    z = np.dot(W.T, a) + b
                    Z[l] = z.copy()
                    A[l] = fun_act(z)

                # Back prop.:
                # 1) ищем ошибку для выходных нейронов
                label = y_true
                y_true = np.zeros((number_neurons_output_layer, 1))
                y_true[label] = 1
                fun_act = self.act[L - 1]
                E[L - 1] = self.err(y_true, A[L - 1], derivative=True) * fun_act(Z[L - 1], derivative=True)
                # 2) теперь ищем ошибки для всех остальных слоёв
                for l in range(L - 2, 0, -1):
                    W = all_W[l + 1]
                    z = Z[l]
                    fun_act = self.act[l]
                    E[l] = np.dot(W, E[l + 1]) * fun_act(z, derivative=True)
                # 3) пересчитываем веса и смещения
                for l in range(L - 1, 0, -1):
                    e = E[l]
                    B[l] -= learn_rate * e
                    all_W[l] -= learn_rate * A[l - 1] * e.T

            self.all_W = all_W
            self.B = B
            self.L = L
            # --- Считаем полные потери в конце каждой 5 эпохи
            if plot_err_fun and epoch % 5 == 0:
                y_pred = self.predict(X)
                loss_mean = 0
                for y_p, y_t in zip(y_pred, y):
                    label = y_t
                    y_t = np.zeros((number_neurons_output_layer, 1))
                    y_t[label] = 1
                    loss_mean += self.err(y_t, y_p)

                loss_mean /= len(y)
                errors.append(float(loss_mean))

        if plot_err_fun:
            plt.title("Error vs epoch")
            plt.plot(range(0, epochs, 5), errors)
            plt.xlabel("Epoch")
            plt.ylabel("Error")
            plt.show()


