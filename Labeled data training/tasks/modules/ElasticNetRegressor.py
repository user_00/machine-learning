import numpy as np


class ElasticNetRegressor:
    def __init__(self, alpha=1, l1_ratio=0.5):
        self.alpha = alpha
        self.l1_ratio = l1_ratio
        self.w = None

    def fit(self, X, y):
        X = ElasticNetRegressor.__add_bias(X)
        self.w = np.random.normal(size=(X.shape[1], 1))

        learn_rate = 0.01
        for epoch in range(100):
            for x, y_true in zip(X, y):
                self.w -= learn_rate * x.reshape(self.w.shape) * self.__error(y_true, np.dot(x, self.w), derivative=True)

        return self

    def predict(self, X):
        return np.dot(ElasticNetRegressor.__add_bias(X), self.w).reshape(len(X))

    @staticmethod
    def __add_bias(X):
        X_new = np.zeros((X.shape[0], X.shape[1] + 1))
        X_new[:, :-1] = X
        X_new[:, -1] = 1
        return X_new

    def __error(self, y_true: float, y_pred: float, derivative=False) -> float:
        """
        Ошибка с учётом l1 и l2 регуляризаций для конкретного объекта
        :param y_true: ответ (число или вектор)
        :param y_pred: предсказание (число или вектор)
        :param derivative: производная функции или нет
        :return ошибка (число или вектор соответственно)
        """
        norm = np.linalg.norm
        if derivative:
            return -2 * (y_true - y_pred) + self.alpha * (self.l1_ratio * (self.w.sum() / norm(self.w, ord=1)) + (1 - self.l1_ratio) * (2 * self.w.sum()))
        return norm(y_true - y_pred) ** 2 + self.alpha * (self.l1_ratio * (norm(self.w, ord=1)) + (1 - self.l1_ratio) * (norm(self.w, ord=2) ** 2))
