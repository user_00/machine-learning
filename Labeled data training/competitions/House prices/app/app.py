import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import os
import streamlit as st
from sklearn.preprocessing import StandardScaler
from xgboost import XGBRegressor

ABSOLUTE_PATH = os.path.dirname(__file__)


def page_description_competition():
    st.title('House Prices - Advanced Regression Techniques')
    st.write('__Predict sales prices and practice feature engineering, RFs, and gradient boosting__')
    st.write('[Страница конкурса на kaggle](https://www.kaggle.com/c/house-prices-advanced-regression-techniques)')
    st.image('https://storage.googleapis.com/kaggle-competitions/kaggle/5407/media/housesbanner.png',
             use_column_width=True)

    st.write(
        '# Описание соревнования'
    )

    st.write(
        'Как гласит описание соревнования – это идеальное соревнование для '
        'студентов, изучающих науку о данных, которые прошли онлайн-курс '
        'машинного обучения и хотят расширить свой набор навыков.'
    )

    st.write(
        'Суть соревнования состоит в том, что участник по 79 независимым '
        'аспектам жилых домов в Эймсе, штат Айова, должен предсказать '
        'окончательную цену каждого дома. В датасет вошли почти все '
        'аспекты жилых домов, как категориальные характеристики, '
        'так порядковые, так и вещественные, например, площадь первого этажа, '
        'площадь второго этажа, вместимость гаража, если таковой имеется, '
        'типы подключенных коммунальных услуг, количество спален,  '
        'качество кухни, тип аллеи, длина фасада и многое другое.'
    )


def page_data_preprocessing():
    st.write(
        '# Предобработка данных'
    )

    st.write(
        'Первое что бросается в глаза после проведения первичного '
        'анализа данных, в датасете имеется большое количество пропущенных '
        'значений. Изучив описание датасета, можно прийти к выводу, что большая '
        'часть признаков, принимающих неизвестное значение, имеет вполне '
        'определённое семантическое значение. Например, признак отвечающий '
        'за качество бассейна имеет пропусти в тех случаях, когда дом не имеет '
        'бассейна, или, например, признак отвечающий за состояние гаража имеет '
        'пропусти в тех случаях, когда дом не имеет гаража. Учитывая это, '
        'данные пропущенные значения были заполнены специальным значением, '
        'обозначающим отсутствие объекта как такового.'
    )

    st.write(
        'Остальные признаки, не имеющие семантического значения и '
        'являющиеся именно пропущенными, неизвестными, были заполнены с '
        'помощью стандартных методов заполнения пропусков в машинном обучении, '
        'а именно заполнением самым популярным вариантом, для категориальных '
        'признаков, и заполнением средним значением, для вещественных.'
    )

    st.write(
        'После заполнения пропущенных значений необходимо обработать признаки. '
        'Исходя из описания датасета, есть один целочисленный признак, который '
        'является на самом деле категориальным и числами лишь кодирует некоторые '
        'категории. Преобразуем его к строковому типу для последующего бинарного '
        'кодирования. Также исходя из описания к датасету, имеются такие '
        'категориальные признаки, которые имеют отношение порядка, необходимо '
        'их преобразовать. Ещё стоит удалить признак обозначающий тип '
        'подключенных коммунальных услуг, так как почти у всех объектов он '
        'принимает одинаковые значения. Напоследок добавим признак обозначающий '
        'итоговую площадь жилища, просуммировав площади первого этажа, второго '
        'этажа и площадь подвала, так как судя по данным, площадь является одним '
        'из важнейших признаков, от которого зависит итоговая стоимость жилья.'
    )

    st.write(
        'Наконец, осталось масштабировать вещественные признаки и '
        'применить one-hot кодирование к категориальным и можно приступать '
        'к построению моделей.'
    )

    st.write(
        ''
    )


def page_building_models():
    st.write('# Построение моделей')

    st.markdown(
        '''
В качестве моделей для построения были выбраны следующие 
модели, как наиболее продвинутые:

* Random forest regressor из библиотеки sklearn
* XGB regressor из библиотеки xgboost;
* CatBoost regressor из библиотеки catboost;
* LGBM regressor из библиотеки lightgbm;
* Sequential из библиотеки keras.

Данные модели и их композиции почти всегда дают лучшие 
результаты на различных соревнованиях по машинному обучению для задач регрессии.
        '''
    )

    st.write(
        'Проведя глубокий анализ выбранных обученных моделей, '
        'была выбрана модель XGB regressor из библиотеки xgboost в качестве основной.'
    )


def page_model_predictions():
    st.write(
        '# Использование модели'
    )

    st.write(
        'Вы можете загрузить данные с информацией об одном или нескольких домах '
        'в формате представленном на странице соревнования и получить предсказанную цену.'
    )

    file = st.file_uploader(label='Загрузите данные для предсказания', type=['csv'], accept_multiple_files=False)
    if file is not None:
        df = pd.read_csv(file)
        st.write('### Исходные данные')
        st.write(df)
        st.write('Размерность датафрейма {}'.format(df.shape))
        st.write('### Данные после обработки')
        df = data_preprocessing(df)
        st.write(df)
        st.write('Размерность датафрейма {}'.format(df.shape))
        model_prediction(df)


def data_preprocessing(df: pd.DataFrame) -> pd.DataFrame:
    if 'SalePrice' in df.columns:
        df.drop(columns='SalePrice', inplace=True)
    if 'Id' in df.columns:
        df.drop(columns='Id', inplace=True)

    # Объединение с данными соревнования для обработки входных данных
    df_len = len(df)
    df = df.append(load_data_for_pretreatment(), ignore_index=True)

    # Обработка пропущенных значений
    features = [
        'PoolQC', 'MiscFeature', 'Alley', 'Fence', 'FireplaceQu',
        'GarageCond', 'GarageQual', 'GarageFinish', 'GarageType',
        'BsmtCond', 'BsmtExposure', 'BsmtQual', 'BsmtFinType2', 'BsmtFinType1'
    ]
    df.loc[:, features] = df.loc[:, features].fillna('Absent')
    df["LotFrontage"] = df.groupby("Neighborhood")["LotFrontage"].transform(
        lambda x: x.fillna(x.median()))
    df['GarageYrBlt'].fillna(-1, inplace=True)
    df['GarageArea'].fillna(0, inplace=True)
    df['GarageCars'].fillna(0, inplace=True)
    df['MasVnrType'].fillna('None', inplace=True)
    df['MasVnrArea'].fillna(0, inplace=True)
    df['MSZoning'].fillna(df['MSZoning'].mode()[0], inplace=True)
    df.drop(columns='Utilities', inplace=True)
    df['Functional'].fillna('Typ', inplace=True)
    features = [
        'BsmtQual', 'BsmtCond', 'BsmtExposure',
        'BsmtFinType1', 'BsmtFinType2'
    ]
    df.loc[:, features] = df.loc[:, features].fillna('Absent')
    features = [
        'BsmtFinSF1', 'BsmtFinSF2', 'BsmtUnfSF',
        'TotalBsmtSF', 'BsmtFullBath', 'BsmtHalfBath'
    ]
    df.loc[:, features] = df.loc[:, features].fillna(0)
    df['Exterior1st'].fillna(df['Exterior1st'].mode()[0], inplace=True)
    df['Exterior2nd'].fillna(df['Exterior2nd'].mode()[0], inplace=True)
    df['SaleType'].fillna(df['SaleType'].mode()[0], inplace=True)
    df['Electrical'].fillna(df['Electrical'].mode()[0], inplace=True)
    df['KitchenQual'].fillna(df['KitchenQual'].mode()[0], inplace=True)
    # Обработка признаков
    df['MSSubClass'] = df['MSSubClass'].apply(str)
    features = [
        'ExterQual', 'ExterCond', 'HeatingQC', 'KitchenQual'
    ]
    for feature in features:
        df[feature].replace(['Po'], 0, inplace=True)
        df[feature].replace(['Fa'], 1, inplace=True)
        df[feature].replace(['TA'], 2, inplace=True)
        df[feature].replace(['Gd'], 3, inplace=True)
        df[feature].replace(['Ex'], 4, inplace=True)
    features = [
        'BsmtQual', 'BsmtCond', 'GarageQual', 'GarageCond', 'FireplaceQu', 'PoolQC'
    ]
    for feature in features:
        df[feature].replace(['Absent'], 0, inplace=True)
        df[feature].replace(['Po'], 1, inplace=True)
        df[feature].replace(['Fa'], 2, inplace=True)
        df[feature].replace(['TA'], 3, inplace=True)
        df[feature].replace(['Gd'], 4, inplace=True)
        df[feature].replace(['Ex'], 5, inplace=True)
    features = [
        'BsmtFinType1', 'BsmtFinType2'
    ]
    for feature in features:
        df[feature].replace(['Absent'], 0, inplace=True)
        df[feature].replace(['Unf'], 1, inplace=True)
        df[feature].replace(['LwQ'], 2, inplace=True)
        df[feature].replace(['Rec'], 3, inplace=True)
        df[feature].replace(['BLQ'], 4, inplace=True)
        df[feature].replace(['ALQ'], 5, inplace=True)
        df[feature].replace(['GLQ'], 6, inplace=True)
    df['Functional'].replace(['Sal'], 0, inplace=True)
    df['Functional'].replace(['Sev'], 1, inplace=True)
    df['Functional'].replace(['Maj2'], 2, inplace=True)
    df['Functional'].replace(['Maj1'], 3, inplace=True)
    df['Functional'].replace(['Mod'], 4, inplace=True)
    df['Functional'].replace(['Min2'], 5, inplace=True)
    df['Functional'].replace(['Min1'], 6, inplace=True)
    df['Functional'].replace(['Typ'], 7, inplace=True)
    df['BsmtExposure'].replace(['Absent'], 0, inplace=True)
    df['BsmtExposure'].replace(['No'], 1, inplace=True)
    df['BsmtExposure'].replace(['Mn'], 2, inplace=True)
    df['BsmtExposure'].replace(['Av'], 3, inplace=True)
    df['BsmtExposure'].replace(['Gd'], 4, inplace=True)
    df['LotShape'].replace(['IR3'], 0, inplace=True)
    df['LotShape'].replace(['IR2'], 1, inplace=True)
    df['LotShape'].replace(['IR1'], 2, inplace=True)
    df['LotShape'].replace(['Reg'], 3, inplace=True)
    df['LandContour'].replace(['Low'], 0, inplace=True)
    df['LandContour'].replace(['HLS'], 1, inplace=True)
    df['LandContour'].replace(['Bnk'], 2, inplace=True)
    df['LandContour'].replace(['Lvl'], 3, inplace=True)
    df['LandSlope'].replace(['Sev'], 0, inplace=True)
    df['LandSlope'].replace(['Mod'], 1, inplace=True)
    df['LandSlope'].replace(['Gtl'], 2, inplace=True)
    df['CentralAir'].replace(['N'], 0, inplace=True)
    df['CentralAir'].replace(['Y'], 1, inplace=True)
    df['GarageType'].replace(['Absent'], 0, inplace=True)
    df['GarageType'].replace(['Detchd'], 1, inplace=True)
    df['GarageType'].replace(['CarPort'], 2, inplace=True)
    df['GarageType'].replace(['BuiltIn'], 3, inplace=True)
    df['GarageType'].replace(['Basment'], 4, inplace=True)
    df['GarageType'].replace(['Attchd'], 5, inplace=True)
    df['GarageType'].replace(['2Types'], 6, inplace=True)
    df['GarageType'].replace(['Absent'], 0, inplace=True)
    df['GarageType'].replace(['Unf'], 1, inplace=True)
    df['GarageType'].replace(['RFn'], 2, inplace=True)
    df['GarageType'].replace(['Fin'], 3, inplace=True)
    df['PavedDrive'].replace(['N'], 0, inplace=True)
    df['PavedDrive'].replace(['P'], 1, inplace=True)
    df['PavedDrive'].replace(['Y'], 2, inplace=True)
    df['Fence'].replace(['Absent'], 0, inplace=True)
    df['Fence'].replace(['MnWw'], 1, inplace=True)
    df['Fence'].replace(['GdWo'], 2, inplace=True)
    df['Fence'].replace(['MnPrv'], 3, inplace=True)
    df['Fence'].replace(['GdPrv'], 4, inplace=True)
    df['TotalSF'] = df['2ndFlrSF'] + df['1stFlrSF'] + df['TotalBsmtSF']
    df['TotalBath'] = 2 * (df['FullBath'] + df['BsmtFullBath']) + df['HalfBath'] + df['BsmtHalfBath']
    df['TotalPorchSF'] = df['WoodDeckSF'] + df['OpenPorchSF'] + df['EnclosedPorch'] + df['3SsnPorch'] + df[
        'ScreenPorch']
    df['HasPool'] = df['PoolArea'].astype(bool).astype(int)
    df['HasGarage'] = df['GarageArea'].astype(bool).astype(int)
    df['HasBsmt'] = df['TotalBsmtSF'].astype(bool).astype(int)
    df['HasFireplace'] = df['Fireplaces'].astype(bool).astype(int)
    df['MoSold'] = df['MoSold'] - df['MoSold'].median()
    df['YrSold'] = df['YrSold'] - df['YrSold'].median()
    df['YearBuilt'] = df['YearBuilt'] - df['YearBuilt'].median()
    df['YearRemodAdd'] = df['YearRemodAdd'] - df['YearRemodAdd'].median()
    df['GarageYrBlt'] = df['GarageYrBlt'] - df['GarageYrBlt'].median()
    features = [
        'LotFrontage', 'LotArea', 'MasVnrArea', 'BsmtFinSF1', 'BsmtFinSF2',
        'BsmtUnfSF', 'TotalBsmtSF', '1stFlrSF', '2ndFlrSF', 'LowQualFinSF',
        'GrLivArea', 'GarageArea', 'WoodDeckSF', 'OpenPorchSF',
        'EnclosedPorch', '3SsnPorch', 'ScreenPorch', 'PoolArea',
        'TotalSF', 'TotalBath', 'TotalPorchSF'
    ]

    sc = StandardScaler()
    sc.fit(df[features])
    df[features] = sc.transform(df[features])
    df = pd.get_dummies(df, drop_first=True).drop(range(df_len, len(df)))
    return df


def model_prediction(X: pd.DataFrame):
    st.write('### Предсказанные значения')
    model = load_model_xgboost()
    pred = model.predict(X)
    del X
    pred = pd.DataFrame(pred, columns=['SalePrice'])
    st.write(pred)

    st.write('### Гистограмма распределения частоты стоимости домов')
    figure = plt.figure()
    sns.distplot(pred, color='green')
    st.pyplot(figure)


def load_model_xgboost():
    model = XGBRegressor()
    model.load_model(ABSOLUTE_PATH + '/models/XGBoost')
    return model


@st.cache
def load_data_for_pretreatment():
    df = pd.read_csv(ABSOLUTE_PATH + '/data/data_for_pretreatment.csv')
    return df


def sidebar():
    return st.sidebar.selectbox(
        'Выберите страницу:',
        ('Описание соревнования',
         'Предобработка данных',
         'Построение моделей',
         'Использование модели'))


def main():
    st.markdown('<style>MainMenu {visibility: hidden;}footer {visibility: hidden;}</style>', unsafe_allow_html=True)
    st.sidebar.title('Страницы')
    page = sidebar()

    if page == 'Описание соревнования':
        page_description_competition()
    elif page == 'Предобработка данных':
        page_data_preprocessing()
    elif page == 'Построение моделей':
        page_building_models()
    elif page == 'Использование модели':
        page_model_predictions()


if __name__ == '__main__':
    main()
